FROM opensuse/leap

RUN zypper -n ar --no-check -p 90 \
      'https://ftp.gwdg.de/pub/linux/misc/packman/suse/openSUSE_Leap_$releasever/' \
      packman && \
    zypper -n --gpg-auto-import-keys ref --build-only && \
    zypper -n in ffmpeg && \
    zypper clean --all
